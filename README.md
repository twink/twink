# twink

silly litl logger for kitties

[![status-badge](https://ci.codeberg.org/api/badges/12503/status.svg)](https://ci.codeberg.org/repos/12503)
[![crates.io](https://img.shields.io/crates/v/twink.svg)](https://crates.io/crates/twink)
[![crates.io](https://img.shields.io/crates/d/twink.svg)](https://crates.io/crates/twink)

```
  ／l、
（ﾟ､ ｡ ７
  l、 ~ヽ
  じしf_,)ノ
```

## usage

```toml
[dependencies]
twink = "1.1.0"
```

```rs
twink::purr!("good job!!!")
```

lso checkout the [example](examples/meow.rs)

### timestamps

to print timestamps next to logs enable the feature flag corresponding to your format preference

#### 12 hour (am/pm)

```toml
twink = { version = "1.1.0", features = ["12-hour"] }
```

#### 24 hour

```toml
twink = { version = "1.1.0", features = ["24-hour"] }
```

### log

twink can also be used as an interface for the [log](https://lib.rs/crates/log) crate

```toml
twink = { version = "1.1.0", features = ["log"] }
```

```rs
twink::log::setup();
log::info!("hiya");
```

(`setup_level` may be used instead to set a filter other than `Info`)

## license

[Apache-2.0](LICENSE)
