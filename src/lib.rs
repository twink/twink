// twink, a little logging crate
// Copyright (c) 2023 fawn and rini
//
// SPDX-License-Identifier: Apache-2.0

pub use twink_macros::fmt;

#[inline]
pub fn __puts(fmt: std::fmt::Arguments) {
    #[cfg(feature = "timestamps")]
    println!(fmt!("<gray>[{}] {}"), local_timestamp(), fmt);
    #[cfg(not(feature = "timestamps"))]
    println!("{fmt}");
}

#[cfg(feature = "log")]
pub mod log;

#[cfg(feature = "12-hour")]
#[inline]
pub fn local_timestamp() -> String {
    ::chrono::Local::now().format("%r").to_string()
}

#[cfg(feature = "24-hour")]
#[inline]
pub fn local_timestamp() -> String {
    ::chrono::Local::now().format("%T").to_string()
}

#[macro_export]
macro_rules! meow {
    ($($fmt:literal)*) => {
        $crate::__puts(::std::format_args!(::std::concat!($($crate::fmt!($fmt)),*)))
    };
    ($($fmt:literal)*, $($args:tt)*) => {
        $crate::__puts(::std::format_args!(::std::concat!($($crate::fmt!($fmt)),*), $($args)*))
    };
}

/// ᵔ ᵕ ᵔ
#[macro_export]
macro_rules! purr {
    () => {
        $crate::meow!("<gray>[<green>+<gray>]</>")
    };
    ($fmt:literal) => {
        $crate::meow!("<gray>[<green>+<gray>]</> " $fmt)
    };
    ($fmt:literal, $($args:tt)*) => {
        $crate::meow!("<gray>[<green>+<gray>]</> " $fmt, $($args)*)
    };
}

/// :3c
#[macro_export]
macro_rules! mrrr {
    () => {
        $crate::meow!("<gray>[<yellow>!<gray>]</>")
    };
    ($fmt:literal) => {
        $crate::meow!("<gray>[<yellow>!<gray>]</> " $fmt)
    };
    ($fmt:literal, $($args:tt)*) => {
        $crate::meow!("<gray>[<yellow>!<gray>]</> " $fmt, $($args)*)
    };
}

/// –ꞈ–
#[macro_export]
macro_rules! hiss {
    () => {
        $crate::meow!("<gray>[<red>-<gray>]</> ")
    };
    ($fmt:literal) => {
        $crate::meow!("<gray>[<red>-<gray>]</> " $fmt)
    };
    ($fmt:literal, $($args:tt)*) => {
        $crate::meow!("<gray>[<red>-<gray>]</> " $fmt, $($args)*)
    };
}

/// ・ヘ・?
#[macro_export]
macro_rules! nyaa {
    () => {
        $crate::meow!("<gray>[<purple>?<gray>]</> ")
    };
    ($fmt:literal) => {
        $crate::meow!("<gray>[<purple>?<gray>]</> " $fmt)
    };
    ($fmt:literal, $($args:tt)*) => {
        $crate::meow!("<gray>[<purple>?<gray>]</> " $fmt, $($args)*)
    };
}
